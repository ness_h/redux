import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import {Router,browserHistory} from 'react-router';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import routes from './routes';
import{loadGoals} from './actions/goalActions';
import{loadUsers} from './actions/userActions';

import './styles/styles.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

//u can override initial state

const store=configureStore();
store.dispatch(loadGoals());
store.dispatch(loadUsers());

render(
  <Provider store={store} >
    <Router history={browserHistory} routes={routes}/></Provider>,
  document.getElementById('app')
);
