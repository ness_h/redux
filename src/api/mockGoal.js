import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const goals = [
  {
    id: "react-flux-building-applications",
    topic: "Building Applications in React and Flux",
    userId: "inesCh",
    category: "JavaScript"
  },
  {
    id: "clean-code",
    topic: "Clean Code: Writing Code for Humans",
    userId: "inesCh",
    category: "Software Practices"
  },
  {
    id: "architecture",
    topic: "Architecting Applications for the Real World",
    userId: "inesCh",
    category: "Software Architecture"
  },
  {
    id: "career-reboot-for-developer-mind",
    topic: "Becoming an Outlier: Reprogramming the Developer Mind",
    userId: "inesCh",
    category: "Career"
  },
  {
    id: "web-components-shadow-dom",
    topic: "Web Component Fundamentals",
    userId: "inesCh",
    category: "HTML5"
  }
];

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (goal) => {
  return replaceAll(goal.topic, ' ', '-');
};

class GoalApi {
  static getAllGoals() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], goals));
      }, delay);
    });
  }

  static saveGoal(goal) {
    goal = Object.assign({}, goal); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minGoalTopicLength = 1;
        if (goal.topic.length < minGoalTopicLength) {
          reject(`Topic must be at least ${minGoalTopicLength} characters.`);
        }

        if (goal.id) {
          const existingGoalIndex = goals.findIndex(a => a.id == goal.id);
          goals.splice(existingGoalIndex, 1, goal);
        } else {
          goal.id = generateId(goal);
          goals.push(goal);
        }

        resolve(goal);
      }, delay);
    });
  }

  static deleteGoal(goalId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfGoalToDelete = goals.findIndex(goal => {
          goal.id == goalId;
        });
        goals.splice(indexOfGoalToDelete, 1);
        resolve();
      }, delay);
    });
  }
}

export default GoalApi;
