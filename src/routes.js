import React from 'react';
import {Route,IndexRoute} from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import AboutPage from './components/about/AboutPage';
import GoalPage from './components/goal/GoalPage';
import ManageGoalPage from './components/goal/ManageGoalPage';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="about" component={AboutPage}/>
    <Route path="goal" component={GoalPage}/>
    <Route path="manage/:id" component={ManageGoalPage}/>
    <Route path="manage" component={ManageGoalPage}/>
  </Route>
);
