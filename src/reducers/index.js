import {combineReducers} from 'redux';
import goals from './goalReducer';
import users from './userReducer';


const rootReducer=combineReducers({
  goals,
  users
});
export default rootReducer;
