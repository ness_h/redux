import * as types from '../actions/actionTypes';
import initialState from './initialState';
export default function goalReducer(state=initialState.goals,action){
  switch(action.type){
    case types.LOAD_GOALS_SUCCESS:
      //state.push(action.goal);
      //return state;
          //not mutable
        //  debugger;
         // return [...state,
           // Object.assign({},action.goal)
            //];
          return action.goals;

      case types.CREATE_GOALS_SUCCESS:
      return [
      ...state,
      Object.assign({},action.goal)];
      case types.UPDATE_GOALS_SUCCESS:
      return [
      ...state.filter(goal=>goal.id!==action.goal.id),
      Object.assign({},action.goal)
      ];
    default:
      return state;
  }
}
