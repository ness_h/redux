import * as types from './actionTypes';
import goalApi from '../api/mockGoal';

export function loadGoalsSuccess(goals){
  return {type:types.LOAD_GOALS_SUCCESS,goals};
}

export function createGoalSuccess(goals){
  return {type:types.CREATE_GOALS_SUCCESS,goals};
}

export function updateGoalSuccess(goals){
  return {type:types.UPDATE_GOALS_SUCCESS,goals};
}

export function loadGoals(){
  return function(dispatch){
    return goalApi.getAllGoals().then(goals => {
      dispatch(loadGoalsSuccess(goals));
    }).catch(error => {
      throw(error);
    });
  };
}
//async : redux thunk , promise and saga
export function saveGoals(goal){
  return function(dispatch,getState){
    return goalApi.saveGoal(goal).then(savedGoal => {
      goal.id ? dispatch(updateGoalSuccess(savedGoal)):
    dispatch(createGoalSuccess(savedGoal));
	}).catch(error => {
      throw(error);
    });
  };
}