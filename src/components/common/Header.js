import React,{PropTypes} from 'react';
import {Link,IndexLink} from 'react-router';
import {
    Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
class Header extends React.Component{

      constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
      render() {
  return(
      <div>
    <div>
        <Navbar color="light"  light expand="md">
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
 <IndexLink to="/" activeClassName="active">Home</IndexLink>
      {" | "}
      </NavItem>
              <NavItem>
 <Link to="/about" activeClassName="active">About</Link>
      {" | "}
      </NavItem>
           <NavItem>
  <Link to="/goal" activeClassName="active">Goals</Link>
      </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
          </div>
  );
}}

export default Header;

