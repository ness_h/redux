import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import Header from './common/Header';
import { Button } from 'reactstrap';


class App extends React.Component{
  render(){
    return (
      <div>
        <Header/>
       <div id="appdev" className="container-fluid">{this.props.children}</div>
      </div>
    ) ;
  }
}

App.propTypes={
  children:PropTypes.object.isRequired
};
export default App;
