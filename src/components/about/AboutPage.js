import React from 'react';
import {Link} from 'react-router';
import {
  UncontrolledCarousel 
} from 'reactstrap'; 

const items = [
  {   
src: 'https://cdn.dribbble.com/users/651412/screenshots/1906337/smart_goals_001.jpg',
      altText: 'Slide 1',
    caption: 'Smart',

  },
  {
    src: 'https://cdn.dribbble.com/users/651412/screenshots/1906337/attachments/325827/smart_goals_004.jpg',
    altText: 'Slide 2',
    caption: 'Attainable',
  },
  {
    src: 'https://cdn.dribbble.com/users/651412/screenshots/1906337/attachments/325826/smart_goals_003.jpg',
    altText: 'Slide 3',
    caption: 'Mesurable'
  }
];


class AboutPage extends React.Component{
    
  render() {
    return (
      <div >
          <UncontrolledCarousel items={items} />
      </div>
    ) ;
  }
}

export default AboutPage;
