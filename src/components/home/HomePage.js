import React from 'react';
import {Link} from 'react-router';
import { Badge , Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';

class HomePage extends React.Component{
  render(){
   return (
     <div id="space">
         <Row>
      <Col sm="6">
        <Card body>
          <CardTitle>About Page</CardTitle>
          <CardText>Click here to be redirected to the About Page</CardText>
<Link to ="about" className="btn btn-primary btn-lg">About</Link>        </Card>
      </Col>
      <Col sm="6">
        <Card body>
          <CardTitle>Goal Page</CardTitle>
          <CardText>Click here to be redirected to the Goals Page</CardText>
<Link to ="goal" className="btn btn-primary btn-lg">Goals</Link>        </Card>
      </Col>
    </Row>
     </div>
   ) ;
  }
}

export default HomePage;
