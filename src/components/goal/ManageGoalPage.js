import React, {PropTypes, Component, Context} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as goalActions from '../../actions/goalActions';
import GoalForm from './GoalForm';


class ManageGoalPage extends React.Component
{
    constructor(props, context)
    {
        super(props, context);
        this.state={
          goal:Object.assign({},this.props.goal),
          errors:{},
          saving:false
        };
        this.updateGoalState=this.updateGoalState.bind(this);
    }
    componentWillReceiveProps(nextProps){
      if(this.props.goal.id != nextProps.goal.id){
        this.setState({goal:Object.assign({},nextProps.goal)});
      }
    }
    updateGoalState(event){
      const field=event.target.name;
      let goal=this.state.goal;
      goal[field]=event.target.value;
      return this.setState({goal:goal});
    }
    saveGoal(event){
      event.preventDefualt();
      this.setState({saving:true});
      this.props.actions.saveGoals(this.state.goal)
      .then(()=>this.redirect());
    }
    redirect(){
       this.context.router.transitionTo('/goal');
    }
    render()
    {
        return (
        <div className="jumbotron">
          <h1>Manage Goal</h1>
          <GoalForm saving={this.state.saving} goal={this.state.goal} onSave={this.saveGoal} errors={this.state.errors} allUsers={this.props.users} onChange={this.updateGoalState}/>
        </div>
        );
    }
}
ManageGoalPage.contextTypes={
  router:PropTypes.object
};

ManageGoalPage.propTypes = {
    //myProp: PropTypes.string.isRequired
  goal:React.PropTypes.object.isRequired,
  users:React.PropTypes.array.isRequired,
  actions:React.PropTypes.object.isRequired
};

function getGoalById(goals,id){
  const goal=goals.filter(goal => goal.id ==id);
  if(goal) return goal[0];
  return null;
}
function mapStateToProps(state, ownProps) {
  const goalId=ownProps.params.id;
  let goal={id:'',topic:'',userId:'',category:''};
  if(goalId && state.goals.length>0){
    goal=getGoalById(state.goals,goalId);
  }
  const usersFormattedForDropdown=state.users.map(user=>{
    return{
      value:user.id,
      text:user.firstName + ' '+ user.lastName
    };
  });
    return {
     goal:goal,
      users:usersFormattedForDropdown
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(goalActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageGoalPage);
