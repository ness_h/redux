import React from 'react';
import {Link , browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as goalActions from '../../actions/goalActions';
import GoalList from './GoalList';
import { Nav, NavItem, NavLink, Table  } from 'reactstrap';


class GoalPage extends React.Component{
  //bind func and initialize
  constructor(props,context){
    super(props,context);
    this.redirectToAddGoalPAge=this.redirectToAddGoalPAge.bind(this);
  }

  goalRow(goal,index){
    return <div key={index}>{goal.topic}</div>;
  }

  redirectToAddGoalPAge(){
    browserHistory.push('manage');
  }

  render(){

    //debugger;
    //{this.props.goals.map(this.goalRow)}
    const {goals}=this.props;
    return (
      <div id="space">
         <Nav>
          <NavItem>
            <NavLink href="#">Goals Page</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">In Progress Goals</NavLink>
          </NavItem>
        <NavItem>
       <input type="submit" value="Add new" className="btn btn-primary" onClick={this.redirectToAddGoalPAge}/>
        </NavItem>
        </Nav><br/>
      <GoalList goals={goals}/>
      </div>
    ) ;
  }
}
//prop validation
GoalPage.propTypes={

  actions:React.PropTypes.object.isRequired,
  goals:React.PropTypes.array.isRequired
};

function mapDispatchTpProps(dispatch){
  return{//manually wrap actions
    //createGoal:goal => dispatch(goalActions.createGoal(goal))
    //map to all actions
    actions:bindActionCreators(goalActions,dispatch)
  };
}

function mapStateToProps(state,ownProps) {
  //debugger;
  return {
    goals:state.goals
  };
}
export default connect(mapStateToProps,mapDispatchTpProps)(GoalPage);
