import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import GoalForm from './GoalForm';

function setup(){
    let props={
        goal:{},saving:false,errors:{},
        onSave:()=>{},
        onChange:()=>{}
    };
    let renderer=TestUtils.createRenderer();
    renderer.render(<GoalForm {...props}/>);
    let output =renderer.getRenderOutput();
return {
props,output,renderer
};
}

describe('GoalForm via React Test Utils',() => {
 it('renders form and h1',()=>{
const {output}=setup();
expect(output.type).toBe('form');
let [TextInput]=output.props.children;
expect(TextInput.type).toBe('TextInput');
 });

 it('save buttin',()=>{
    const {output}=setup(false);
    const submitButton=output.props.children[3];
    expect(submitButton.props.value).toBe('Save');
    
     });
});

//with enzyme : shallow then expect wrapper.find
//less code and more readable 
//just find function to get sthn









