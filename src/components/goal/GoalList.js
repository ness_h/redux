import React, {PropTypes} from 'react';
import GoalListRow from './GoalListRow';
import {  Table  } from 'reactstrap';

const GoalList=({goals})=> {
  return(
    <table className="table">
      <thead>
      <tr>
        <th>Topic</th>
        <th>Category</th>
        <th>User</th>
      </tr>
      </thead>
      <tbody>
      {goals.map(goal =>
      <GoalListRow key={goal.id} goal={goal} />)}
      </tbody>
    </table>
  );
};

GoalList.propTypes={
  goals:PropTypes.array.isRequired
};

export default GoalList;
