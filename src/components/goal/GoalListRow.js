import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const GoalListRow=({goal})=> {
  return(
   <tr>
     <td>
       <Link to={'/manage/'+goal.id}>{goal.topic}</Link>
     </td>
     <td>
       {goal.category}
     </td>
     <td>
       {goal.userId}
     </td>
   </tr>
  );
};

GoalListRow.propTypes={
  goal:PropTypes.object.isRequired
};

export default GoalListRow;
