import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';

const GoalForm= ({goal,allUsers,onSave,onChange,saving,errors}) => {
  return (
    <form>
      <TextInput name="topic" label="Topic" value={goal.topic} onChange={onChange} error={errors.topic}/>
      <SelectInput name="userId" label="User" value={goal.userId} defaultOption="Select User" options={allUsers} onChange={onChange} error={errors.userId}/>
      <TextInput name="category" label="Category" value={goal.category} onChange={onChange} error={errors.category}/>
      <input type="submit" disabled={saving} value={saving ? 'SAVING..' : 'Save'} className="btn btn-primary" onClick={onSave}/>
    </form>
  );
};

GoalForm.propTypes={
  goal:React.PropTypes.object.isRequired,
  allUsers:React.PropTypes.array,
  onSave:React.PropTypes.func,
  onChange:React.PropTypes.func,
  loading:React.PropTypes.bool,
  errors:React.PropTypes.object,
  saving:React.PropTypes.bool
};

export default GoalForm;
