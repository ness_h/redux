import React from 'react';
import expect from 'expect';
import {mount,shallow}from 'enzyme';
import ManageGoalPage from './ManageGoalPage';

describe ('Manage Goal Page', ()=>{
    it('sets message when trying to save empty', ()=>{
        const props={
            users:[],
            actions:{saveGoal:()=>{return Promise.resolve();}},
            goal:{id:'',topic:'',userId:'',category:''}
        };
        const wrapper = mount(<ManageGoalPage users={[]}/>
                             );
        const saveButton=wrapper.find('input').last();
        expect(saveButton.prop('type')).toBe('submit');
        saveButton.simulate('click');
        expect(wrapper.state().errors.title).toBe('shouldnt be less than 5 ');
     
    });
});